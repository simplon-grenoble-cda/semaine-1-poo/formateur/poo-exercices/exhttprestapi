On veut créer une API REST pour représenter des articles et des commentaires.

[[_TOC_]]

# Description du modèle métier

On manipule deux types d'entités  :

- Des articles : constitués d'un ID, d'un titre, du texte de l'article, de leur date de publication et du nom de leur auteur.
- Des commentaires : associés à un article, constitués d'un ID, du texte du commentaire, du pseudo du commentateur et de la date de publication.

![](model.png)

# Étapes de réalisation

## 1. Écrire la spécification de votre API web, en suivant la convention REST. 

Il faut des endpoints pour pouvoir manipuler les articles : lister les articles, obtenir les données d'un seul article, 
créer un article, mettre à jour un article, supprimer un article.

Les endpoints pour manipuler les commentaires (les commentaires sont associés aux articles) : 
lister les commentaires d'un article, créer un commentaire associé à un article, mettre à jour un commentaire, supprimer un commentaire.

Pour chaque endpoint il faut spécifier : le path, le corps de la requête si nécessaire en cas de requête POST, le corps de la réponse,
les code de retours éventuels en cas d'erreur.

Exemple de la doc d'une API professionnelle (Twitter) : https://developer.twitter.com/en/docs/twitter-api/tweets/lookup/api-reference/get-tweets-id

Exemple de la spec de l'API pour le projet morpion via HTTP : https://gitlab.com/simplon-grenoble-cda/poo-exercices/exhttptictactoe

## 2. Prenez en main la base de données SQLite qui contient les tables permettant de stocker les articles et les commentaires.

Je vous ai fournit un squelette de base de donnée dans le fichier `database.sqlite`.

## 3. Pensez au cas particuliers : 

- Quand on supprime un article, il faut bien supprimer les commentaires associés
- Les cas d'erreurs, si une requête est appelée avec de mauvais paramètres : 

  - un article dont l'ID n'existe pas
  - une requête de création ou de mise à jour avec des paramètres inadaptés (propriétés manquantes)

## 4. Implémentez votre API en Java

- Mettre en plae un serveur HTTP
- Créer des handlers pour les différents paths de votre API
- Pour chaque handler, faire les actions qui permettent de construire la réponse appopriée : 

    - se connecter à la base de données
    - faire les requêtes SQL qu'il faut (en lecture ou en écriture, selon la requête)
    - construire une réponse au format JSON à envoyer en réponse HTTP

## 5. Testez votre API Java avec un client HTTP 

- En mode graphique : Postman
- En ligne de commande : `curl`
